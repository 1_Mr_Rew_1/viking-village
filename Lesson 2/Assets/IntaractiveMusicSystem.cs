﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;
using Invector.vCharacterController;


public class IntaractiveMusicSystem : MonoBehaviour
{
    [FMODUnity.EventRef] public string musicEvent;
    private FMOD.Studio.EventInstance musicInstance;
    public vThirdPersonController player;


    public vControlAIMelee enemy;
    public bool inCombat = false;

    // Start is called before the first frame update
    void Start()
    {
        musicInstance = FMODUnity.RuntimeManager.CreateInstance(musicEvent);
        musicInstance.start();

    }

    // Update is called once per frame
    void Update()
    {
        musicInstance.setParameterByName("HealthBar", player.currentHealth);

        if (!inCombat && enemy.isInCombat)

        {
            musicInstance.setParameterByName("GameState", 1f);
            Debug.Log("Combat");
            inCombat = true;
        }
        else if (inCombat && enemy.isInCombat == false)
        {
            musicInstance.setParameterByName("GameState", 0f);
            Debug.Log("Exploration");
            inCombat = false;  

        }
        if (player.isDead == true)
        {
            musicInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
    }
}
