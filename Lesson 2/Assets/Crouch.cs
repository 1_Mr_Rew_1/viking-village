﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using FMOD.Studio;

public class Crouch : MonoBehaviour
{

    public vThirdPersonController tpController;
    [FMODUnity.EventRef] public string HealthSnapshot;
    
   
    FMOD.Studio.EventInstance SnapInstance;
    public FMOD.Studio.PLAYBACK_STATE state;

    void Start()
    {
        tpController = GetComponent<vThirdPersonController>();
        SnapInstance = FMODUnity.RuntimeManager.CreateInstance(HealthSnapshot);
        
    }
    void Update()
    {
        //Debug.Log(tpController.currentHealth);
        SnapInstance.getPlaybackState(out state);
       
        if (tpController.isGrounded)
        {
            if (state != FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                SnapInstance.start();
                

            }

        }

      
       
        if (tpController.isDead == true)
        {
           
            SnapInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }




    }

    
}
