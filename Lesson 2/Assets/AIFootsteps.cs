using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;

public class AIFootsteps : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string walkingEvent;
    [FMODUnity.EventRef]
    public string runningEvent;

    FMOD.Studio.EventInstance walkingInstance;

    public LayerMask lm;
    float surface;
    

    vAIMotor Magnitude;
    // Start is called before the first frame update
    void Start()
    {
        Magnitude = gameObject.GetComponent<vAIMotor>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void footstep()
    {
         SurfaceCheck();

        if (Magnitude.input.magnitude > 0.1)

            
            
                {
                    walkingInstance = FMODUnity.RuntimeManager.CreateInstance(runningEvent);
                    FMODUnity.RuntimeManager.AttachInstanceToGameObject(walkingInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
                    
                    walkingInstance.setParameterByName("surface_type", surface);
                    walkingInstance.start();
                    walkingInstance.release();
                }

                else
                {
                    walkingInstance = FMODUnity.RuntimeManager.CreateInstance(walkingEvent);
                    FMODUnity.RuntimeManager.AttachInstanceToGameObject(walkingInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
                    
                    walkingInstance.setParameterByName("surface_type", surface);
                    walkingInstance.start();
                    walkingInstance.release();
                }
        


        void SurfaceCheck()
    {
      
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            Debug.Log(hit.collider.tag);
    
            switch (hit.collider.tag)
            {
                case "Snow":
                    surface = 0f;
                    break;
                case "Wood":
                    surface = 1f;
                    break;
                case "Water":
                    surface = 2f;
                    break;
                default:
                    surface = 0f;
                    break;
            }

        }

    }




    }
}
