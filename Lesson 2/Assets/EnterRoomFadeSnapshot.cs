﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using FMOD.Studio;

public class EnterRoomFadeSnapshot : MonoBehaviour
{
    public vThirdPersonController controller;

    [FMODUnity.EventRef]
    public string snapshotevent;
    FMOD.Studio.EventInstance snapshotinstance;

    // Start is called before the first frame update
    void Start()
    {
        controller = gameObject.GetComponent<vThirdPersonController>();
        snapshotinstance = FMODUnity.RuntimeManager.CreateInstance(snapshotevent);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            snapshotinstance.start();
            

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            snapshotinstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
           
        }

    }
}
