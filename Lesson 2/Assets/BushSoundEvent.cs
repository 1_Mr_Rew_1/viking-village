﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BushSoundEvent : MonoBehaviour

{
    [FMODUnity.EventRef] public string Bushevent;
    private FMOD.Studio.EventInstance BushInstance;

    // Start is called before the first frame update
    void Start()
    {
        BushInstance = FMODUnity.RuntimeManager.CreateInstance(Bushevent);

        FMODUnity.RuntimeManager.AttachInstanceToGameObject(BushInstance, gameObject.GetComponent<Transform>(), gameObject.GetComponent<Rigidbody>());
        BushInstance.start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
