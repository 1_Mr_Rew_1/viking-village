using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damageScript : MonoBehaviour
{
    [FMODUnity.EventRef] public string HitDamageEvent;
    [FMODUnity.EventRef] public string FootKickEvent;
    [FMODUnity.EventRef] public string HitDamageShoutsEvent;
    [FMODUnity.EventRef] public string deadEvent;

    //[FMODUnity.EventRef]
    public void HitDamage()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(HitDamageEvent, gameObject); 
    }   

    public void HitDamageShouts()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(HitDamageShoutsEvent, gameObject);
    }
    public void footKick()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(HitDamageShoutsEvent, gameObject);
    }
    public void Dead()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(deadEvent, gameObject);
    }

}
