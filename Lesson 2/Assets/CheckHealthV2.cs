using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using FMOD.Studio;

public class CheckHealthV2 : MonoBehaviour
{
    public vThirdPersonController tpController;
    [FMODUnity.EventRef] public string HealthSnapshot;
    [FMODUnity.EventRef] public string HeartbeatEvent;
    FMOD.Studio.EventInstance HeartbeatInstance;
    FMOD.Studio.EventInstance SnapInstance;
    public FMOD.Studio.PLAYBACK_STATE state;

    // Start is called before the first frame update
    void Start()
    {
        tpController = GetComponent<vThirdPersonController>();
        SnapInstance = FMODUnity.RuntimeManager.CreateInstance(HealthSnapshot);
        HeartbeatInstance = FMODUnity.RuntimeManager.CreateInstance(HeartbeatEvent);
        HeartbeatInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));

        //HeartbeatInstance.start();
        //FMODUnity.RuntimeManager.PlayOneShotAttached(HeartbeatEvent, gameObject);

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(tpController.currentHealth);
        SnapInstance.getPlaybackState(out state);
        HeartbeatInstance.getPlaybackState(out state);
        HeartbeatInstance.setParameterByName("HealthBar", tpController.currentHealth);

        if (tpController.currentHealth <= 30)
        {
            if (state != FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                SnapInstance.start();
                HeartbeatInstance.start();

            }

        }

        else if (tpController.currentHealth > 30)
        {
            if (state == FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                SnapInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                HeartbeatInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }

        }
        if (tpController.isDead == true)
        {
            HeartbeatInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            SnapInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
    }
}