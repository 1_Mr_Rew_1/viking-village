using System;
using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;
using UnityEngine;
using Invector.vCharacterController;

public class healthchecker : MonoBehaviour
{
    public vThirdPersonController tpController;
    [FMODUnity.EventRef] public string HealthSnapshot;

    private FMOD.Studio.EventInstance SnapInstace;
    public FMOD.Studio.PLAYBACK_STATE state;


    void Start()
    {
        tpController = GetComponent<vThirdPersonController>();
        SnapInstace = FMODUnity.RuntimeManager.CreateInstance(HealthSnapshot);
    }

    

    public void HealthSnapshotStart()
    {
        SnapInstace.getPlaybackState(out state);
        if (state != PLAYBACK_STATE.PLAYING)
        {
            SnapInstace.start();
        }
    }

    public void HealthSnapshotStop()
    {
        SnapInstace.getPlaybackState(out state);
        if (tpController.currentHealth > 60)
        {
            if (state == PLAYBACK_STATE.PLAYING)
            {
                SnapInstace.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }
        }
        
    }
}
