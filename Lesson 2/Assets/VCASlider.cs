using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VCASlider : MonoBehaviour
{
    private UnityEngine.UI.Slider ourSlider; // приватная переменная для доступа к слайдеру
    private FMOD.Studio.VCA ourVCA; // переменная для доступа к установки и получению громкости с фмод
    private float vcaVolume; // в эту переменную записывается значение громкости из VCA
    public string VCAName; // Название VCA-ручки

    void Start()
    {
        ourSlider = gameObject.GetComponent<UnityEngine.UI.Slider>(); // переменной слайдера добавляем доступ к компоненту слайдера
        ourVCA = FMODUnity.RuntimeManager.GetVCA("vca:/" + VCAName); // даем доступ к VCA по имени, которое мы пишем в компоненте
        ourVCA.getVolume(out vcaVolume); // получаем значение ручки VCA из фмода
        ourSlider.value = vcaVolume; //  передаем значение громкости шины VCA в переменную vcaVolume
    }


    public void VCAVolumeChange()
    {
        ourVCA.setVolume(ourSlider.value); // устанавливаем громкость шины VCA в такое же значение, как значение нашего слайдера
        /*ourVCA.getVolume(out vcaVolume);
        Debug.Log(vcaVolume);*/
    }
}
