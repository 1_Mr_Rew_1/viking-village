﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class villagezone : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string villageEvent;
    [FMODUnity.EventRef]
    public string forestEvent;

    FMOD.Studio.EventInstance villageInstance;
    FMOD.Studio.EventInstance forestInstance;
   


    // Start is called before the first frame update
    void Start()
    {
        villageInstance = FMODUnity.RuntimeManager.CreateInstance(villageEvent);
        forestInstance = FMODUnity.RuntimeManager.CreateInstance(forestEvent);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            
            villageInstance.start();
            forestInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            villageInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            forestInstance.start();
        }

    }
}
