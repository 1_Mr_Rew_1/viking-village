﻿using Invector.vCharacterController;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armswhoosh : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string whooshEvent;
    [FMODUnity.EventRef]
    public string whooshlowEvent;
   

    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void whoosh()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(whooshEvent, gameObject);

    }
    void whooshlow()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(whooshlowEvent, gameObject);

    }
}

